﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ConsultantStatsReporter;
using ArgyllHelpers;
using System.IO;

namespace ConsultantStatsReporterNUnitTests
{
    [TestFixture]
    public class ProgramTests
    {
        string logsFolder = @"C:\testdata\ConsultantStatsReporter\logs";
        string debugLog = @"C:\testdata\ConsultantStatsReporter\logs\debug.log";
        string errorLog = @"C:\testdata\ConsultantStatsReporter\logs\error.log";
        string alertLog = @"C:\TestData\ConsultantStatsReporter\logs\Alert.log";
        string familyConfigFolder = @"C:\testdata\ConsultantStatsReporter\ProductConfig\Family";
        string productConfigFolder = @"C:\testdata\ConsultantStatsReporter\ProductConfig\Product";
        string testFamilyConfigFolder = @"\\corp\Landmark\Groups\Argyll_Data\Argyll\IT\Systems\Production\ProductConfigurations\Test\Family";
        string testProductConfigFolder = @"\\corp\Landmark\Groups\Argyll_Data\Argyll\IT\Systems\Production\ProductConfigurations\Test\Product";        

        public void CleanUpAllTheThings()
        {
            FolderFileStoreHelper.DeleteFolderContents(logsFolder);
        }

        [TestFixtureSetUp]
        public void CleanUp()
        {
            CleanUpAllTheThings();
            FolderFileStoreHelper.DeleteFolderContents(familyConfigFolder);
            FolderFileStoreHelper.DeleteFolderContents(productConfigFolder);

            ArgyllHelpers.UnitTesting.UnitTestHelpers.CopyAllFilesInTheFolder(testFamilyConfigFolder, familyConfigFolder);
            ArgyllHelpers.UnitTesting.UnitTestHelpers.CopyAllFilesInTheFolder(testProductConfigFolder, productConfigFolder);
        }

        [TestCase]

        public void Main()
        {
            Program.Main();

            Assert.False(File.Exists(errorLog), "error log");
            Assert.True(File.Exists(alertLog), "Alert file exists");
            Assert.True(FolderFileStoreHelper.FileContents(alertLog).Contains("Brief"), "No brief in output");
        }

        [TestCase]

        public void GetAndSetConfig_NoSpecifiedConfig()
        {
            CleanUpAllTheThings();
            Program.GetAndSetConfig(String.Empty);
            Assert.True(File.Exists(debugLog), "debug log");
            Assert.True(FolderFileStoreHelper.FileContents(debugLog).Contains("Using standard config"), FolderFileStoreHelper.FileContents(debugLog));
        }


        [TestCase]

        public void GetAndSetConfig_NonExistantConfigFile()
        {
            CleanUpAllTheThings();
            var ex = Assert.Throws(typeof(FileNotFoundException), (() => Program.GetAndSetConfig(@"C:\TestData\ConsultantStatsReporter\config\idontexist")), "Unexpected error");
            Assert.True(ex.Message.Contains("specified as the config file cannot be found"), "Exception message was " + ex.Message);
            Assert.True(File.Exists(errorLog), "error log");
        }

        [TestCase]

        public void GetAndSetConfig_ConfigFileHasEmptyMasterConfig()
        {
            CleanUpAllTheThings();
            string configFile = @"C:\TestData\ConsultantStatsReporter\Config\masterConfigNotSpecified.config";
            Program.GetAndSetConfig(configFile);
            Assert.True(File.Exists(debugLog), "debug log");
            Assert.True(FolderFileStoreHelper.FileContents(debugLog).Contains(configFile), FolderFileStoreHelper.FileContents(debugLog));
            Assert.True(File.Exists(errorLog), "error log");
            Assert.True(FolderFileStoreHelper.FileContents(errorLog).Contains("The Master Config File path must be specified"), "master config error " + FolderFileStoreHelper.FileContents(errorLog));
        }

        [TestCase]

        public void GetAndSetConfig_ConfigFilePointsAtInvalidMasterConfig()
        {
            CleanUpAllTheThings();            
            string configFile = @"C:\TestData\ConsultantStatsReporter\Config\masterConfigNotExist.config";
            Program.GetAndSetConfig(configFile);
            Assert.True(File.Exists(debugLog), "debug log");
            Assert.True(FolderFileStoreHelper.FileContents(debugLog).Contains(configFile), FolderFileStoreHelper.FileContents(debugLog));
            Assert.True(File.Exists(errorLog), "error log");
            Assert.True(FolderFileStoreHelper.FileContents(errorLog).Contains("for the Master Config File path doesn't appear to be a valid file"), "master config error");
        }

        [TestCase]

        public void GetAndSetConfig_ConfigFilePointsAtMasterConfigInvalidFamilyFolder()
        {
            CleanUpAllTheThings();            
            string configFile = @"C:\TestData\ConsultantStatsReporter\Config\masterConfigInvalidFamilyFolder.config";
            Program.GetAndSetConfig(configFile);
            Assert.True(File.Exists(debugLog), "debug log");
            Assert.True(FolderFileStoreHelper.FileContents(debugLog).Contains(configFile), FolderFileStoreHelper.FileContents(debugLog));

            Assert.True(File.Exists(errorLog), "error log");
            Assert.True(FolderFileStoreHelper.FileContents(errorLog).Contains("family config folder not specified"), "master config error");
        }

        [TestCase]

        public void GetAndSetConfig_ConfigFilePointsAtMasterConfigInvalidProductFolder()
        {
            CleanUpAllTheThings();            
            string configFile = @"C:\TestData\ConsultantStatsReporter\Config\masterConfigInvalidProductFolder.config";
            Program.GetAndSetConfig(configFile);
            Assert.True(File.Exists(debugLog), "debug log");
            Assert.True(FolderFileStoreHelper.FileContents(debugLog).Contains(configFile), FolderFileStoreHelper.FileContents(debugLog));
            Assert.True(File.Exists(errorLog), "error log");
            Assert.True(FolderFileStoreHelper.FileContents(errorLog).Contains("product config folder not specified"), "master config error");
        }
    }
}
