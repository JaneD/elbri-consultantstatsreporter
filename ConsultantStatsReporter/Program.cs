﻿// ---------------------------------
// <copyright file="Program.cs" company="Argyll Environmental">
//     Copyright Argyll Environmental
// </copyright>
// <author>Jane Dallaway</author>
// ---------------------------------

namespace ConsultantStatsReporter
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using ArgyllHelpers;
    using NLog;

    /// <summary>
    /// the main starting point for the stats viewer
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// nLog logging
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// the path to the master config file
        /// </summary>
        private static string masterConfigFile;

        /// <summary>
        /// the folder where product configs are stored
        /// </summary>
        private static string productConfigFolder;

        /// <summary>
        /// the folder where family configs are stored
        /// </summary>
        private static string familyConfigFolder;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>        
        public static void Main()
        {
            // Get the configuration
            logger.Log(LogLevel.Debug, "About to load config");
            GetAndSetConfig(String.Empty);

            // Get the Argyll Data 
            logger.Log(LogLevel.Debug, "About to get the data");
            SortedDictionary<string, int> data = GetArgyllData();

            // Output the numbers
            logger.Log(LogLevel.Debug, "About to output the data");
            OutputFamilyFigures(data);
        }

        /// <summary>
        /// Get the argyll data
        /// </summary>
        /// <returns>a dictionary of family codes and the number of orders that fit that</returns>
        internal static SortedDictionary<string, int> GetArgyllData()
        {
            try
            {
                // This is needed to get the webservices to work in the Exeter environment. Can be set by config. But trying to set by code so it is less likely to be forgotten.
                System.Net.ServicePointManager.Expect100Continue = false;

                // get the values from the web services
                WebToolsWS.webtools webToolsWS = new WebToolsWS.webtools();
                try
                {
                    WebToolsWS.OrderDetailsSummary[] ordersToWrite = webToolsWS.GetOrdersReadyToWrite("ST");
                    SortedDictionary<string, int> familyOrders = new SortedDictionary<string, int>();

                    foreach (WebToolsWS.OrderDetailsSummary order in ordersToWrite)
                    {
                        string productCode = GeneralHelpers.GetStemProductCodeFromReference(order.OrderRef);
                        ProductConfig pc = ProductConfig.LoadConfig(productConfigFolder, familyConfigFolder, productCode);
                        if (familyOrders.ContainsKey(pc.Family))
                        {
                            familyOrders[pc.Family]++; 
                        }
                        else
                        {
                            familyOrders.Add(pc.Family, 1);
                        }
                    }

                    return familyOrders;
                }
                catch (Exception ex)
                {
                    logger.Log(LogLevel.Debug, "Error raised trying to get the orders to write " + ex.Message + ", " + ex.StackTrace);
                    throw;
                }
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Error, "Error {0}", ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get and set the config settings
        /// </summary>
        /// <param name="configFile">the file to load - if empty use the default</param>
        internal static void GetAndSetConfig(string configFile)
        {
            if (!string.IsNullOrEmpty(configFile))
            {
                logger = LogManager.GetLogger("Program.GetAndSetConfig");
                logger.Log(LogLevel.Debug, "Looking for config file at {0}", configFile);

                if (!File.Exists(configFile))
                {
                    logger = LogManager.GetLogger("Program.GetAndSetConfig");
                    logger.Log(LogLevel.Error, "Can't find config file at {0}", configFile);
                    throw new FileNotFoundException(string.Format(CultureInfo.InvariantCulture, "The file {0} specified as the config file cannot be found", configFile));
                }
            }

            Configuration config;
            AppSettingsSection appSettings;
            if (!string.IsNullOrEmpty(configFile))
            {
                logger = LogManager.GetLogger("Program.GetAndSetConfig");
                logger.Log(LogLevel.Debug, "Loading config from {0}", configFile);

                ExeConfigurationFileMap exeMap = new ExeConfigurationFileMap();
                exeMap.ExeConfigFilename = configFile;
                config = ConfigurationManager.OpenMappedExeConfiguration(exeMap, ConfigurationUserLevel.None);
            }
            else
            {
                logger = LogManager.GetLogger("Program.GetAndSetConfig");
                logger.Log(LogLevel.Debug, "Using standard config");
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }

            appSettings = config.AppSettings;

            masterConfigFile = appSettings.Settings["masterConfigFile"].Value;
            if (string.IsNullOrEmpty(masterConfigFile))
            {
                logger.Log(LogLevel.Fatal, "The Master Config File path must be specified.");
            }
            else
            {
                if (!File.Exists(masterConfigFile))
                {
                    logger.Log(LogLevel.Fatal, string.Concat("The file path specified (" + masterConfigFile + ") for the Master Config File path doesn't appear to be a valid file. Please check and update if appropriate."));
                }
                else
                {
                    MasterConfig mc = MasterConfig.LoadConfig(masterConfigFile);

                    productConfigFolder = mc.ProductConfigFolder;
                    if (string.IsNullOrEmpty(productConfigFolder))
                    {
                        logger.Log(LogLevel.Fatal, "product config folder not specified. Must be. Check config in {0}.", masterConfigFile);
                    }

                    familyConfigFolder = mc.FamilyConfigFolder;
                    if (string.IsNullOrEmpty(familyConfigFolder))
                    {
                        logger.Log(LogLevel.Fatal, "family config folder not specified. Must be. Check config in {0}.", masterConfigFile);
                    }
                }
            }            
        }

        /// <summary>
        /// Output the family level data
        /// </summary>
        /// <param name="familyOrders">A dictionary populated with families and number of orders</param>
        internal static void OutputFamilyFigures(SortedDictionary<string, int> familyOrders)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string familyCode in FamilyConfig.GetFamilyCodesThereIsConfigFor(familyConfigFolder))
            {
                if (familyOrders.ContainsKey(familyCode))
                {
                    string familyCodeOutput = string.Format(CultureInfo.InvariantCulture, "{0} : {1}\r\n", familyCode.Replace("_"," "), familyOrders[familyCode]);
                    sb.Append(familyCodeOutput);
                }
            }

            logger = LogManager.GetLogger("ordersToWrite");
            logger.Log(LogLevel.Warn, sb.ToString());
            logger = LogManager.GetCurrentClassLogger();
        }
    }
}
