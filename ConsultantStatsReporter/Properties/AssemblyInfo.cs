﻿// ---------------------------------
// <copyright file="AssemblyInfo.cs" company="Argyll Environmental">
//     Copyright Argyll Environmental
// </copyright>
// <author>Jane Dallaway</author>
// ---------------------------------

using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ConsultantStatsReporter")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Argyll Environmental Ltd")]
[assembly: AssemblyProduct("ConsultantStatsReporter")]
[assembly: AssemblyCopyright("Copyright © Argyll Environmental Ltd 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6499e276-a0ad-442c-b9a5-8ebd7d081619")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.0.0")]
[assembly: CLSCompliant(false)]
[assembly: NeutralResourcesLanguageAttribute("en")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1016:MarkAssembliesWithAssemblyVersion", Justification = "Will be given an assembly version number as part of the Jenkins Process.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames", Justification = "No GAC deploy, so not needed")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("ConsultantStatsReporterNUnitTests")]