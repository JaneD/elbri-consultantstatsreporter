@ECHO OFF

C:\jenkinsutils\OpenCover\OpenCover.Console.exe -target:"C:\Program Files (x86)\NUnit 2.6\bin\nunit-console.exe" -targetargs:"/nologo /noshadow C:\ConsultantStatsReporter\ConsultantStatsReporterNUnitTests\ConsultantStatsReporter.nunit" -filter:"+[ConsultantStatsReporter]* -[*Core*]* -[*]*.*Properties*.* -[*]*.*Designer*.* -[nunit-*]* -[*]*.*WS*.*" -register:user -output:outputCoverage.xml -hideskipped:Filter 

ECHO *** nUnit Tests Completed ***

C:\jenkinsutils\OpenCoverToCoberturaConverter\OpenCoverToCoberturaConverter.exe -input:outputCoverage.xml -output:outputCobertura.xml -sources:"C:\ConsultantStatsReporter\"

ECHO *** Converted to Cobertura ***

MKDIR CodeCoverageHTML
C:\jenkinsutils\CoverageReportGenerator\bin\ReportGenerator.exe -reports:outputCoverage.xml -targetDir:CodeCoverageHTML

ECHO *** HTML produced ***